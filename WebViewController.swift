//
//  WebViewController.swift
//  WorldTrotter
//
//  Created by LiGuo on 15/12/31.
//  Copyright © 2015年 Fruit Lee. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {
    
    var webView: UIWebView!
    
    override func loadView() {
        webView = UIWebView()
        self.view = webView
        
        let targetURL = NSURL(string: "http://www.bignerdranch.com")
        let request = NSURLRequest(URL: targetURL!)
        webView.loadRequest(request)
    }
}
