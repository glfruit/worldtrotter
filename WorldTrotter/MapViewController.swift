//
//  ViewController.swift
//  WorldTrotter
//
//  Created by LiGuo on 15/12/30.
//  Copyright © 2015年 Fruit Lee. All rights reserved.
//

import UIKit
import MapKit

import Fabric
import Crashlytics

class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    var mapView: MKMapView!

    var locationMgr: CLLocationManager = CLLocationManager()

    // MARK: - Overriden 
    override func loadView() {
        mapView = MKMapView()
        mapView.delegate = self
        mapView.userTrackingMode = .Follow

        locationMgr = CLLocationManager()
        locationMgr.delegate = self
        locationMgr.desiredAccuracy = kCLLocationAccuracyBest
        locationMgr.distanceFilter = 5

        if CLLocationManager.authorizationStatus() == .NotDetermined {
            locationMgr.requestWhenInUseAuthorization()
        }


        self.view = mapView

        let standardString = NSLocalizedString("Standard", comment: "Standard map view")
        let satelliteString = NSLocalizedString("Statellite", comment: "Satellite map view")
        let hybridString = NSLocalizedString("Hybrid", comment: "Hybrid map view")

        let segmentedControl = UISegmentedControl(items: [standardString, satelliteString, hybridString])
        segmentedControl.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.5)
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false

        self.view.addSubview(segmentedControl)

        let topConstraint = segmentedControl.topAnchor.constraintEqualToAnchor(topLayoutGuide.bottomAnchor, constant: 8)
//        let leadingConstraint = segmentedControl.leadingAnchor.constraintEqualToAnchor(view.leadingAnchor)
//        let trailingConstraint = segmentedControl.trailingAnchor.constraintEqualToAnchor(view.trailingAnchor)

        topConstraint.active = true

        let margins = view.layoutMarginsGuide
        let leadingConstraint = segmentedControl.leadingAnchor.constraintEqualToAnchor(margins.leadingAnchor)
        let trailingConstraint = segmentedControl.trailingAnchor.constraintEqualToAnchor(margins.trailingAnchor)
        leadingConstraint.active = true
        trailingConstraint.active = true

        segmentedControl.addTarget(self, action: "mapTypeChanged:", forControlEvents: .ValueChanged)

        let locatingButton = UIButton()
        locatingButton.setTitle("Current Location", forState: .Normal)
        locatingButton.setTitleColor(UIColor.blueColor(), forState: .Normal)
        // Important configuration!!!
        locatingButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(locatingButton)

        let btnCenterConstraint = locatingButton.centerXAnchor.constraintEqualToAnchor(view.centerXAnchor)
        // TODO: What does bottomLayoutGuide.topAnchor mean?
        let btnBottomConstraint = locatingButton.topAnchor.constraintEqualToAnchor(bottomLayoutGuide.topAnchor, constant: -38)

        btnCenterConstraint.active = true
        btnBottomConstraint.active = true

        locatingButton.addTarget(self, action: "currentLocation:", forControlEvents: .TouchUpInside)


    }

    func currentLocation(sender: UIButton!) {
        let region = MKCoordinateRegionMake(mapView.userLocation.location!.coordinate, MKCoordinateSpanMake(0.01, 0.01))
        mapView.setRegion(region, animated: true)
        log("Current user location: \(mapView.userLocation.location)")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewWillAppear(animated: Bool) {
        log("MapView showUserLocation is set to true")

        self.mapView.showsUserLocation = true
    }

    override func viewWillDisappear(animated: Bool) {
        self.mapView.showsUserLocation = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - CLLocationManagerDelegate
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        self.mapView.showsUserLocation = true
        manager.startUpdatingLocation()
    }

    func mapView(mapView: MKMapView, didFailToLocateUserWithError error: NSError) {
        log("Failed to locate user: \(error.description)")
    }

    func mapView(mapView: MKMapView, didUpdateUserLocation userLocation: MKUserLocation) {
        log("User location updated:\(userLocation.location)")
    }

    // MARK: - Member functions
    func mapTypeChanged(segControl: UISegmentedControl) {
        switch segControl.selectedSegmentIndex {
        case 0:
            mapView.mapType = .Standard
        case 1:
            mapView.mapType = .Hybrid
        case 2:
            mapView.mapType = .Satellite
        default:
            break
        }
    }

    func log(format: String, args: CVarArgType...) {
        #if DEBUG
                CLSNSLogv(format, getVaList([]))
        #else
                CLSLogv(format, getVaList([]))
        #endif
    }
}

