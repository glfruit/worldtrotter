//
//  ConversionViewController.swift
//  WorldTrotter
//
//  Created by LiGuo on 15/12/30.
//  Copyright © 2015年 Fruit Lee. All rights reserved.
//

import UIKit

class ConversionViewController : UIViewController, UITextFieldDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet var celsiusLabel : UILabel!
    
    @IBOutlet var textField : UITextField!
    
    // MARK: - Constants
    let numberFormatter : NSNumberFormatter = {
        let nf = NSNumberFormatter()
        nf.numberStyle = .DecimalStyle
        nf.minimumFractionDigits = 0
        nf.maximumFractionDigits = 1
        return nf
    }()
    
    // MARK: - Properties
    var fahrenheitValue : Double? {
        didSet {
            updateCelsiusLabel()
        }
    }
    
    // MARK: - Computed Properties
    var celsiusValue : Double? {
        if let value = fahrenheitValue {
            return (value - 32) * (5/9)
        } else {
            return nil
        }
    }
    
    // MARK: - IBAction
    @IBAction func fahrenheitFieldEditingChanged(textField: UITextField) {
        if let text = textField.text, number = numberFormatter.numberFromString(text) {
            fahrenheitValue = number.doubleValue
        } else {
            fahrenheitValue = nil
        }
    }
    
    @IBAction func dismissKeyboard(sender: AnyObject) {
        textField.resignFirstResponder()
    }
    
    // MARK: - UITextFieldDelegate
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let currentLocale = NSLocale.currentLocale()
        let decimalSeparator = currentLocale.objectForKey(NSLocaleDecimalSeparator) as! String

        let existingTextHasDecimalSeparator = textField.text?.rangeOfString(decimalSeparator)
        let replaceTextHasDecimalSepeartor = string.rangeOfString(decimalSeparator)
        if existingTextHasDecimalSeparator != nil && replaceTextHasDecimalSepeartor != nil {
            return false
        }
        
        let decimalCharacter = NSCharacterSet.decimalDigitCharacterSet()
        let validChars = NSMutableCharacterSet(charactersInString: ".")
        validChars.formUnionWithCharacterSet(decimalCharacter)
        let nonDecimalCharacter = validChars.invertedSet
        let rangeOfNonDecimalCharacter = (string as NSString).rangeOfCharacterFromSet(nonDecimalCharacter)
        if rangeOfNonDecimalCharacter.location != NSNotFound {
            return false
        }
        
        return true
    }
    
    // MARK: - Member Functions
    func updateCelsiusLabel() {
        if let value = celsiusValue {
            celsiusLabel.text = numberFormatter.stringFromNumber(value)
        } else {
            celsiusLabel.text = "???"
        }
    }
    
}
